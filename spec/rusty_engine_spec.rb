# frozen_string_literal: true

require 'spec_helper'

RSpec.describe RustyEngine do
  describe '.convert' do
    let(:converted_file) { 'spec/images/converted.png' }
    subject { RustyEngine.convert('spec/images/foo.jpg', converted_file) }

    after do
      File.delete(converted_file) if File.exist?(converted_file)
    end

    it 'convert image' do
      subject
      expect(File.exist?(converted_file)).to be true
    end
  end

  describe '.sort' do
    let(:sorted_file) { 'spec/images/sorted.png' }
    subject do
      RustyEngine.sort('spec/images/foo.jpg', sorted_file, '1', 'true', '0', '0', '30', 'true', '60', '100', '0')
    end

    after do
      File.delete(sorted_file) if File.exist?(sorted_file)
    end

    it 'pixel sort image' do
      subject
      expect(File.exist?(sorted_file)).to be true
    end
  end

  describe '.brush' do
    let(:brushed_file) { 'spec/images/brushed.png' }
    subject do
      RustyEngine.brush('spec/images/foo.jpg', brushed_file, '90', '5', '10', '2')
    end

    after do
      File.delete(brushed_file) if File.exist?(brushed_file)
    end

    it 'pixel brush image' do
      subject
      expect(File.exist?(brushed_file)).to be true
    end
  end

  describe '.slim' do
    let(:slimed_file) { 'spec/images/slimed.png' }
    subject do
      RustyEngine.slim('spec/images/foo.jpg', slimed_file, '90', 'global', '3', 'red', '99')
    end

    after do
      File.delete(slimed_file) if File.exist?(slimed_file)
    end

    it 'pixel slim image' do
      subject
      expect(File.exist?(slimed_file)).to be true
    end
  end
end
