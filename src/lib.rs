extern crate libc;
extern crate arko;

use libc::c_char;
use std::ffi::CStr;

#[no_mangle]
pub extern "C" fn convert(input: *const c_char, output: *const c_char) {
    let in_img = c_string_to_rust_string(input);
    let out_img = c_string_to_rust_string(output);
    arko::convert(in_img, out_img, true);
}

#[no_mangle]
pub extern "C" fn brush(input: *const c_char, output: *const c_char,
                        c_prob: *const c_char, c_min: *const c_char,
                        c_max: *const c_char, c_direction: *const c_char) {
    let in_img = c_string_to_rust_string(input);
    let out_img = c_string_to_rust_string(output);
    let proba = c_string_to_rust_int(c_prob);
    let min = c_string_to_rust_int(c_min);
    let max = c_string_to_rust_int(c_max);
    let direction = c_string_to_rust_int(c_direction);
    match direction {
        1 => arko::brush_left_to_right(in_img, out_img, true, proba, min, max),
        2 => arko::brush_top_to_bottom(in_img, out_img, true, proba, min, max),
        3 => arko::brush_right_to_left(in_img, out_img, true, proba, min, max),
        _ => arko::brush_bottom_to_top(in_img, out_img, true, proba, min, max),
    }
}

#[no_mangle]
pub extern "C" fn slim(input: *const c_char, output: *const c_char,
                       c_prob: *const c_char, c_probability_area: *const c_char,
                       c_direction: *const c_char, c_colors: *const c_char,
                       c_colors_proba: *const c_char) {
    let in_img = c_string_to_rust_string(input);
    let out_img = c_string_to_rust_string(output);
    let proba = c_string_to_rust_int(c_prob);
    let probability_area = c_string_to_rust_string(c_probability_area);
    let direction = c_string_to_rust_int(c_direction);
    let colors = c_colors_to_rust_colors(c_colors);
    let colors_proba = c_colors_proba_to_rust_colors_proba(c_colors_proba);

    if probability_area == "global" {
        match direction {
            1 => arko::slim_top_to_bottom_global(in_img, out_img, true, proba, colors),
            2 => arko::slim_bottom_to_top_global(in_img, out_img, true, proba, colors),
            3 => arko::slim_left_to_right_global(in_img, out_img, true, proba, colors),
            _ => arko::slim_right_to_left_global(in_img, out_img, true, proba, colors),
        }
    } else {
        match direction {
            1 => arko::slim_top_to_bottom_per_color(in_img, out_img, true, colors_proba),
            2 => arko::slim_bottom_to_top_per_color(in_img, out_img, true, colors_proba),
            3 => arko::slim_left_to_right_per_color(in_img, out_img, true, colors_proba),
            _ => arko::slim_right_to_left_per_color(in_img, out_img, true, colors_proba),
        }
    }
}

#[no_mangle]
pub extern "C" fn sort(input: *const c_char, output: *const c_char,
                       c_direc: *const c_char, c_smart: *const c_char,
                       c_detection: *const c_char, c_min: *const c_char,
                       c_max: *const c_char, c_multi: *const c_char,
                       c_min_2: *const c_char, c_max_2: *const c_char,
                       c_sorting_by: *const c_char) {
    let in_img = c_string_to_rust_string(input);
    let out_img = c_string_to_rust_string(output);
    let direction = c_string_to_rust_int(c_direc);
    let smart_sorting = c_string_to_rust_bool(c_smart);
    let detection_type = c_string_to_rust_int(c_detection);
    let min = c_string_to_rust_int(c_min);
    let max = c_string_to_rust_int(c_max);
    let multiple_range = c_string_to_rust_bool(c_multi);
    let min_2 = c_string_to_rust_int(c_min_2);
    let max_2 = c_string_to_rust_int(c_max_2);
    let sorting_by = c_string_to_rust_int(c_sorting_by);

    if smart_sorting {
        match direction {
            1 => arko::sort_top_to_bottom(in_img, out_img, true, detection_type, min, max, multiple_range, min_2, max_2, sorting_by),
            2 => arko::sort_bottom_to_top(in_img, out_img, true, detection_type, min, max, multiple_range, min_2, max_2, sorting_by),
            3 => arko::sort_left_to_right(in_img, out_img, true, detection_type, min, max, multiple_range, min_2, max_2, sorting_by),
            _ => arko::sort_right_to_left(in_img, out_img, true, detection_type, min, max, multiple_range, min_2, max_2, sorting_by),
        }
    } else {
        match direction {
            1 => arko::sort_brut_top_to_bottom(in_img, out_img, true),
            2 => arko::sort_brut_bottom_to_top(in_img, out_img, true),
            3 => arko::sort_brut_left_to_right(in_img, out_img, true),
            _ => arko::sort_brut_right_to_left(in_img, out_img, true),
        }
    }
}

fn c_string_to_rust_string<'a>(c_string: *const c_char) -> &'a str {
    let c_str = unsafe {
        assert!(!c_string.is_null());

        CStr::from_ptr(c_string)
    };

    let rust_string = c_str.to_str().unwrap();
    return rust_string
}

fn c_string_to_rust_int<'a>(c_string: *const c_char) -> i32 {
    let rust_int = c_string_to_rust_string(c_string).to_string().parse::<i32>().unwrap();
    return rust_int
}

fn c_string_to_rust_bool<'a>(c_string: *const c_char) -> bool {
    let rust_string = c_string_to_rust_string(c_string);
    return rust_string == "true" || rust_string == "TRUE";
}

fn c_colors_to_rust_colors<'a>(c_string: *const c_char) -> Vec<&'a str> {
    let color_string = c_string_to_rust_string(c_string);
    color_string.split(',').collect()
}

fn c_colors_proba_to_rust_colors_proba<'a>(c_string: *const c_char) -> Vec<(&'a str, i32)> {
    let colors_proba_string = c_string_to_rust_string(c_string);
    let colors_proba_vector = colors_proba_string.split(',');
    colors_proba_vector.map(|x| colors_proba_to_tuple(x)).collect()
}

fn colors_proba_to_tuple<'a>(colors_proba: &str) -> (&str, i32) {
    let mut colors_proba_vector = colors_proba.split(':');
    let color : &str = colors_proba_vector.next().unwrap_or("");
    let proba : i32 = colors_proba_vector.next().unwrap_or("0").parse::<i32>().unwrap();
    (color, proba)
}
