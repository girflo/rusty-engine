Gem::Specification.new do |s|
  s.name          = 'rusty_engine_ffi'
  s.version       = '0.1.0'
  s.summary       = 'Tiny and fast image manipulation library'
  s.description   = <<-EOT
    The Rust Engine is a tiny library written in rust to perform simple images manipulations.
    For now the library included four functions :
    - Image converter
    - Pixel sorting
    - Brush and Slim : two algorithms that will mess with the given image
  EOT
  s.required_ruby_version     = '>= 2.0.0'
  s.required_rubygems_version = '>= 1.8.11'
  s.add_runtime_dependency 'ffi', '~> 1.15', '>= 1.15.0'

  s.authors       = ['Flo Girardo']
  s.email         = 'florian@barbrousse.net'
  s.files         = Dir['lib/*'] + Dir['lib/bin/*']
  s.test_files    = ['spec']
  s.homepage      =
    'https://gitlab.com/girflo/rusty-engine'
  s.license       = 'MIT'
  s.add_development_dependency('rspec', '~> 3')
end
