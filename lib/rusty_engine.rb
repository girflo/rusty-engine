# frozen_string_literal: true

require 'ffi'

# main file for Rusty Engine gem
module RustyEngine
  extend FFI::Library
  VERSION = '0.0.4'

  lib_name = "bin/librusty_engine.#{FFI::Platform::LIBSUFFIX}"
  ffi_lib File.expand_path(lib_name, __dir__)

  # Convert png and jpg images
  # @param input_image [String]
  # @param output_image [String]
  attach_function :convert, %i[string string], :void, { blocking: true }

  # Apply brush algorithm to given image
  # Params : input, output, proba, min, max, direction
  # @param input_image [String]
  # @param output_image [String]
  # @param probability [String] should be between '0' and '100'
  # @param min [String] should be between '0' and the max param
  # @param max [String] should be above the min param
  # @param directions [String] '1' -> horizontal
  #                            '2' -> vertical
  #                            '3' -> horizontal_inverted
  #                            '4' -> vertical_inverted
  attach_function :brush, %i[string string string string string string], :void, { blocking: true }

  # Apply slim algorithm to given image
  # Params : input, output, proba, probability_area, direction, colors, colors_proba
  # @param input_image [String]
  # @param output_image [String]
  # @param probability [String] should be between '0' and '100'
  # @param probability_area [String] either 'global' or 'local'
  # @param directions [String] '1' -> up_to_down
  #                            '2' -> down_to_up
  #                            '3' -> left_to_right
  #                            '4' -> right_to_left
  # @param colors [String]
  # @param colors_proba [String]
  attach_function :slim, %i[string string string string string string string], :void, { blocking: true }

  # Pixel sort the given image
  # Params : input, output, direction, smart_sorting, detection_type, min, max, multiple_range, min_2, max_2, sorting_by
  # @param input_image [String]
  # @param output_image [String]
  # @param directions [String] 0 -> up_to_down
  #                            1 -> down_to_up
  #                            2 -> left_to_right
  #                            3 -> right_to_left
  # @param smart_sorting [String] 'true' or 'false'
  # @param detection_type [String] 0 -> hues
  #                                1 -> colors
  # @param min [String] should be between '0' and the max param
  # @param max [String] should be above the min param
  # @param multiple_range [String] 'true' or 'false'
  # @param min_2 [String] should be between '0' and the max param
  # @param max_2 [String] should be above the min param
  # @param sorting_by [String] 0 -> hue
  #                            1 -> saturation
  attach_function :sort, %i[string string string string string string string string string string string],
                  :void, { blocking: true }
end
